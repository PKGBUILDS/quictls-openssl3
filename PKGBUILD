# Maintainer: ObserverOfTime <chronobserver@disroot.org>
# Based on quictls-openssl

pkgname=quictls-openssl3
pkgver=3.1.4+quic
pkgrel=1
pkgdesc="TLS/SSL and crypto library with QUIC APIs, replacement for OpenSSL 3.1."
arch=('x86_64')
url="https://github.com/quictls/openssl"
license=('custom:BSD')
depends=('glibc')
makedepends=('perl' 'git')
optdepends=('ca-certificates' 'perl')
conflicts=('openssl' 'quictls-openssl')
provides=("openssl=${pkgver%+quic}"
          "quictls-openssl=${pkgver}"
          'libcrypto.so=3-64' 'libssl.so=3-64')
backup=('etc/ssl/openssl.cnf')
source=("git+${url}#branch=openssl-${pkgver}" "ca-dir.patch")
sha256sums=('SKIP'
            '0a32d9ca68e8d985ce0bfef6a4c20b46675e06178cc2d0bf6d91bd6865d648b7')

prepare() {
  cd "$srcdir/openssl"

  # set CA dir to /etc/ssl by default
  patch -p1 -i "$srcdir/ca-dir.patch"

  sed -i VERSION.dat -e '/^SHLIB_VERSION=/ s/81\.3$/3/'
}

build() {
  cd "$srcdir/openssl"

  # mark stack as non-executable: http://bugs.archlinux.org/task/12434
  ./Configure --prefix=/usr --openssldir=/etc/ssl --libdir=lib \
    shared enable-ktls enable-ec_nistp_64_gcc_128 linux-x86_64 \
    "-Wa,--noexecstack" ${CPPFLAGS} ${CFLAGS} ${LDFLAGS}

  make depend
  make -j$(nproc)
}

check() {
  cd "${srcdir}/openssl"

  # the test fails due to missing write permissions in /etc/ssl
  # revert this patch for make test
  patch -p1 -R -i "${srcdir}/ca-dir.patch"

  make HARNESS_JOBS=$(nproc) test

  patch -p1 -i "${srcdir}/ca-dir.patch"

  # re-run make to re-generate CA.pl from the patched .in file.
  make apps/CA.pl
}

package() {
  cd "${srcdir}/openssl"

  make DESTDIR="${pkgdir}" MANDIR=/usr/share/man \
    MANSUFFIX=ssl install_sw install_ssldirs install_man_docs

  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licences/${pkgname}/LICENSE.txt"
}
